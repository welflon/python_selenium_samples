import time

from selenium import webdriver
from selenium.webdriver import Keys
from selenium.webdriver.common.by import By
from selenium.webdriver.support.wait import WebDriverWait
from selenium.webdriver.support.expected_conditions import visibility_of_element_located

driver = webdriver.Chrome()
driver.get(url="https://helion.pl/")

print("Allow cookie")
WebDriverWait(driver, timeout=5).until(visibility_of_element_located((By.ID, "CybotCookiebotDialog")))
driver.find_element(By.ID, 'CybotCookiebotDialogBodyLevelButtonLevelOptinAllowAll').click()

print("Find python book")
print("Use search with key word: Python")
WebDriverWait(driver, timeout=5).until(lambda x: x.find_element(By.ID, "inputSearch"))
driver.find_element(By.ID, "inputSearch").send_keys('python' + Keys.ENTER)
print("Wait 2 seconds")
print("Get first element and go to the details")

WebDriverWait(driver, timeout=5).until(
    lambda x: x.find_element(By.LINK_TEXT, "Python. Instrukcje dla programisty. Wydanie III"))


driver.find_element(By.LINK_TEXT, "Python. Instrukcje dla programisty. Wydanie III").click()
# TODO  should use filters - first element cannot exist
print("Open preview ")
driver.find_element(By.ID, 'zajrzyj').click()
time.sleep(2)
